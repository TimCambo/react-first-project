import React, {useState} from "react";
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
//import Item from "./Components/Item/Item";

function App() {
  //const [monState, setMonState] = useState(10)
  const [toggle, setToggle] = useState(true)
  
  const changeState = () => {
    //le but est de changer la valeur par l'inverse de celle déjà présente
    setToggle(!toggle)
  }
 {/** short circuit operator (avec les boolean) => toggle && <h1>State True</h1> 
 Affiche le h1 que si le toggle est true sinon affiche rien */}
 //attention le jsx(le return) accepte des declaration => operation +,-,* mais pas de expression => if()..
  return (
    <div>
      <div className={toggle ? "box animated" : "box"}></div>
      {toggle ? <h1>State True</h1> : <h1>State false</h1>}
        <br/>
        <button onClick={changeState}>Changer de state</button>
    </div>
  );
  
}
export default App;

function List() {
  const [dataArr, setDataArr] = useState([
    {nom: "Juliette"},
    {nom: "John"},
    {nom: "Joris"},
  ])
 return (
 <div className="List">
  {dataArr.map((item, index) => {
    //pour avoir un id specifique pour chaque item, ajout de key
    return <p key={index}>Nom: {item.nom}</p>
  })}
</div>
 );
}

export {List};

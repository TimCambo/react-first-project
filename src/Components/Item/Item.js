import './Item.css'
//import imgVelo from '../../assets/imgVelo.jpg'
import {useState} from "react";

function Item() {

    //const toggle = true;
    // ajout de style css via const 
    const styleCss = {fontFamily: "sans-serif", fontSize: "50px"}
    const myLetter = "g"
    const [inputData, setInputData] = useState()

    const changeInput = (e) => {
        setInputData(e)
    }
    console.log(inputData);

    return (
        <div>
            {/* opération ternaire utiles pour animation avec les states*/}
            <h1 className="titre-item" style={styleCss}>Hello depuis Item</h1>
            {/* meilleur façon d'utiliser les images c'est de les importer via import, en les variabilisant */}
            {/* optimisé pour le cache avec webpack, et pour la mimification
            <img src={imgVelo} alt="img velo"/>
            */}
            
            {/* Pour plusieurs images ayant besoin de dynamiser le nom -> dossier public,
            ne pes oublie de renseigner la racine du projet (PUBLIC_URL)
        */}
        <img src={process.env.PUBLIC_URL + `./imgVelo.jp${myLetter}`} alt="img velo"/>
            {/*A partir de onInput on peut utiliser les event Listener*/}
            <input type="text" onInput={e => changeInput(e.target.value)}/>
        </div>
    );
}
export default Item;
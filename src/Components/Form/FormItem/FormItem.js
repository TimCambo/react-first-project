

export default function FormItem(props) {
  return (
    <li className="border d-flex justify-content-between align-items-center p-2 m-2">
        <div className="p-3">{props.txt}</div>
        <button 
        className="btn btn-danger p-2 h-50"
        //on met une fonction anonyme pour empecher que l'action ne s'execute au chargement de la page
        onClick={() => props.delFunq(props.id)}
        >Supprimer</button>
    </li>
  )
}

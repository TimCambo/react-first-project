import { useState } from "react";
import FormItem from "./FormItem/FormItem";
import { v4 as uuidv4 } from 'uuid';

export default function Form() {
    const [dataArr, setDataArr] = useState([
        {txt: "Promener le chien", id: uuidv4()},
        {txt: "Sport", id: uuidv4()},
        {txt: "react", id: uuidv4()}
    ])

    const deleteElement = id => {
        //return les data dont l'id n'est pas celui renseigné
        const filterState = dataArr.filter(item => {
            return item.id !== id;
        })
        setDataArr(filterState);
    }
    const [stateInput, setStateInput] = useState();
    const linkedInput = e => {
        setStateInput(e);
    }
    const addTodo = e => {
        //lors de l'envoi d'un formulaire cela va empecher d' actualiser la page
        e.preventDefault();
        // ... est le spread operator = copie ce qu'il y a dans dataArr (immutabilité)
        const newArr = [...dataArr]
        const newTodo = {};
        newTodo.txt = stateInput
        newTodo.id = uuidv4();
        newArr.push(newTodo)
        setDataArr(newArr)
        setStateInput('')
    }
    return (
        <div className="m-auto px-4 col-12 col-sm-10 col-lg-6">
            <form onSubmit={e => addTodo(e)} className="mb-3">
                <label htmlFor="todo" className="form-label mt-3">Chose à faire</label>
                <input
                value={stateInput}
                onInput={e => linkedInput(e.target.value)}
                id="todo" 
                type="text" 
                className="form-control" />
                <button className="mt-2 btn btn-primary d-block">Envoyer</button>
            </form>
            <h2>Liste des choses a faire</h2>
            <ul className="list-group">
                {dataArr.map((item) => {
                    return (
                        <FormItem 
                        txt={item.txt}
                        key={item.id}
                        id={item.id}
                        delFunq={deleteElement}
                        />
                    )
                })}
            </ul>
        </div>
    )
}
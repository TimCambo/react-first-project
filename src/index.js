import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//import App, { List } from './App';
import TodoList from './TodoList';

ReactDOM.render(
  <React.StrictMode>
   {/*
   <App />
    <List />
   */}
   <TodoList/>
   
  </React.StrictMode>,
  document.getElementById('root')
);
